# t80219j

The public git repository for my Open University MSc dissertation code.

## Prerequisites

Ubuntu:

  sudo apt install tshark python3-pip

  pip3 install numpy pandas keras tensorflow
