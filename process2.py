# See README.md

# Runs on CPUs with AVX extension only (TensorFlow requirement)

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import socket
import struct
import sys
import os.path
import math
from random import randint # needed for randint
import re

# Process the arguments

if len(sys.argv) < 4:
    print("Incorrect number of arguments")
    sys.exit(1)

virusdatacsv = sys.argv[1]
inertdatacsv = sys.argv[2]
maliciousurl = sys.argv[3]

options = ''

if len(sys.argv) == 5:
    options = sys.argv[4]

if os.path.isfile(virusdatacsv):
    print ("Virus CSV File: ", virusdatacsv)
else:
    print("File does not exist (virus CSV).")
    sys.exit(1)

if os.path.isfile(inertdatacsv):
    print ("Inert CSV File: ", inertdatacsv)
else:
    print("File does not exist (inert CSV).")
    sys.exit(1)

if os.path.isfile(maliciousurl):
    print ("Malicious URL File: ", maliciousurl)
else:
    print("File does not exist (Malicious URL).")
    sys.exit(1)

# Read the training and test data

# Read a comma-separated values (csv) file into DataFrame.
virusdataset = pd.read_csv(virusdatacsv)
inertdataset = pd.read_csv(inertdatacsv)

# Read malicious URL list
with open(maliciousurl) as f:
    maliciousurllist = f.read().splitlines()

# Before starting lets clean the data.

def dns_qry_prefixes(s):
    return s.count('.')

def dns_qry_malicious(s):
    if s in maliciousurllist:
        return 1.0
    return 0.0

def dns_qry_num_num(s):
    return sum(c.isdigit() for c in s)

def dns_qry_length(s):
    return len(s)

def dns_qry_name_longest_substr(s):
    try:
        mx = 0
        for n in re.split(r'[\.\-_0-9]\s*', s):
            if len(n) > mx:
                mx = len(n)
        return mx
    except:
        print("Error in dns_qry_name_longest_substr with: ")
        print(s)

def dns_resp_ttl_avg(s):
    if isinstance(s, str):
        sm = 0
        for n in s.split(','):
            n = float(n)
            sm += n
        return sm / len(s.split(','))
    if isinstance(s, float):
        if math.isnan(s):
            return 0.0
        return s

def num_distinct_ip_in_response(s):
    if isinstance(s, float):
        return 0.0
    if isinstance(s, str):
        count = 0
        for n in s.split(','):
            count += 1
        return count

def dns_resp_ip_diversity(s):
    if isinstance(s, float):
        return 0.0
    if isinstance(s, str):
        arr = []
        for n in s.split(','):
            arr.append(ip2int(n))
    return np.std(arr)

def ip2int(ip):
    try:
        packedIP = socket.inet_aton(ip)
    except:
        print("ERROR")
        print(ip)
    return struct.unpack("!L", packedIP)[0]

def CleanData(data):
    # Convert IP addresses to numbers
    data['ip_src_num'] = data.ip_src.apply(ip2int)
    data['ip_dst_num'] = data.ip_dst.apply(ip2int)
    # Average the dns_resp_ttl column
    data['dns_resp_ttl_avg'] = data.dns_resp_ttl.apply(dns_resp_ttl_avg)
    # Length of longest substring
    data['dns_qry_name_longest_substr'] = data.dns_qry_name.apply(dns_qry_name_longest_substr)
    # Number of Distinct IP addresses returned
    data['num_distinct_ip_in_response'] = data.dns_a.apply(num_distinct_ip_in_response)
    # IP Address Diversity of responses
    data['dns_resp_ip_diversity'] = data.dns_a.apply(dns_resp_ip_diversity)
    # Query length
    data['dns_qry_length'] = data.dns_qry_name.apply(dns_qry_length)
    # Number of numerical characters in query
    data['dns_qry_num_num'] = data.dns_qry_name.apply(dns_qry_num_num)
    # Is DNS Query to malicious domain
    data['dns_qry_malicious'] = data.dns_qry_name.apply(dns_qry_malicious)
    # Number of prefixes
    data['dns_qry_prefixes'] = data.dns_qry_name.apply(dns_qry_prefixes)
    # Delete useless columns
    data = data.drop(columns= ["ip_src"])
    data = data.drop(columns= ["ip_dst"])
    data = data.drop(columns= ["dns_a"])
    data = data.drop(columns= ["dns_qry_name"])
    data = data.drop(columns= ["dns_resp_ttl"])
    # Fill all the NaNs as 0
    data = data.fillna(0)
    return data

virusdataset = CleanData(virusdataset)
inertdataset = CleanData(inertdataset)

# Add 'result'
virusdataset['malware'] = 1
inertdataset['malware'] = 0

# Merge the datasets
dataset  = pd.concat([virusdataset, inertdataset], axis=0)
print("Merged Shape (observations, variables):")
print(dataset.shape)
print()

# Normalization
# normalization of the predictors via scaling between 0 and 1. This is needed to eliminate the influence of the predictor's units and magnitude on the modelling process.

target_column = ['malware'] 
predictors = list(set(list(dataset.columns))-set(target_column))
dataset[predictors] = dataset[predictors]/dataset[predictors].max()

# Pick out your feature column(s)
xdataset = dataset[['dns_flags_response', 'dns_qry_malicious', 'dns_qry_prefixes', 'dns_qry_num_num', 'dns_qry_length', 'dns_resp_ip_diversity', 'num_distinct_ip_in_response', 'dns_qry_name_longest_substr', 'dns_resp_ttl_avg']]

# Pick your result column
ydataset = dataset[['malware']]

# Split the data into a training and validation set
msk = np.random.rand(len(xdataset)) < 0.8
x_validation = xdataset[~msk]
x_train = xdataset[msk]
y_validation = ydataset[~msk]
y_train = ydataset[msk]

print("X Validation Dataset Shape (observations, variables):")
print(x_validation.shape)
print("X Train Dataset Shape (observations, variables):")
print(x_train.shape)
print("Y Validation Dataset Shape (observations, variables):")
print(y_validation.shape)
print("X Train Dataset Shape (observations, variables):")
print(y_train.shape)

from keras.utils import to_categorical 

y_train = to_categorical(y_train)
y_validation = to_categorical(y_validation)

count_classes = y_validation.shape[1]
print(count_classes)

print('Validation data elements:', len(x_validation))
print('Training elements:', len(x_train))

if options != "--nomodel":

    from keras.models import Sequential
    from keras.layers import Dense, Activation

    # Create the model 

    model = Sequential()
    model.add(Dense(500, activation="relu", input_dim=9))
    model.add(Dense(200, activation="relu")) # Hidden Layer 
    model.add(Dense(2, activation="softmax")) # Output Layer

    # Compile the model

    model.compile(optimizer='adam', loss = "binary_crossentropy", metrics = ['accuracy'])

    # Fit the model with the training dataset

    model.fit(x_train, y_train, epochs = 32)

    # Evaluate the model with the validation dataset
  
    pred_test = model.predict(x_validation)
    scores2 = model.evaluate(x_validation, y_validation, verbose=0)
    print('Accuracy on validation data: {}% \n Error on test data: {}'.format(scores2[1], 1 - scores2[1]))    

    # serialize model to JSON
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.weights")
    print("Saved model to disk")
