#!/bin/bash

# Remove intermediary files:
rm -f test-data.csv
rm -f pcaps.pcap

# Merge all the virus PCAPs into one pcap:

mergecap -w viruspcaps.pcap pcaps/*/*.pcap

# Process the virus PCAP file, and produce the CSV

if [ ! -f virusdata.csv ]; then

cat features.csv > virusdata.csv

tshark -T fields \
    -e ip.src \
    -e ip.dst \
    -e udp.srcport \
    -e udp.dstport \
    -e dns.flags.response \
    -e dns.count.queries \
    -e dns.count.answers \
    -e dns.qry.name \
    -e dns.qry.name.len \
    -e dns.resp.ttl \
    -e dns.a \
    -e dns.time \
    -E quote=d -E separator=, -Y 'dns' -r viruspcaps.pcap >> virusdata.csv

fi

# Process the inert pacp file, and produce the CSV

if [ ! -f inertdata.csv ]; then

cat features.csv > inertdata.csv

tshark -T fields \
    -e ip.src \
    -e ip.dst \
    -e udp.srcport \
    -e udp.dstport \
    -e dns.flags.response \
    -e dns.count.queries \
    -e dns.count.answers \
    -e dns.qry.name \
    -e dns.qry.name.len \
    -e dns.resp.ttl \
    -e dns.a \
    -e dns.time \
    -E quote=d -E separator=, -Y 'dns' -r inertpcap.pcap >> inertdata.csv

fi

# Secondary processing, and training the neural network

python3 ./process2.py virusdata.csv inertdata.csv malicious_domains.txt
